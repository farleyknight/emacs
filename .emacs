
;;
;; TODO (6/26/2011): Learn enough elisp to break this file up into multiple sections
;;

(setq mac-option-key-is-meta nil)
(setq mac-command-key-is-meta t)
(setq mac-command-modifier 'meta)
(setq mac-option-modifier 'alt)

;; Do not make friggin backups
(setq make-backup-files nil)

;; Custom emacs backup file configuration
(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
     backup-by-copying t            ; Don't delink hardlinks
     version-control t              ; Use version numbers on backups
     delete-old-versions t          ; Automatically delete excess backups
     kept-new-versions 20           ; how many of the newest versions to keep
     kept-old-versions 5)           ; and how many of the old


(add-to-list 'load-path "~/.emacs.d/")

;;; Global stuff
(if (not (string-match "XEmacs\\|Lucid" emacs-version))
    (global-font-lock-mode 1)
	(font-lock-mode t))

(font-lock-mode)
(server-mode)

;; color-theme
(add-to-list 'load-path "~/.emacs.d/color-theme-6.6.0")
(require 'color-theme)

(eval-after-load "color-theme"
  '(progn
     (color-theme-initialize)
     (color-theme-dark-laptop)))

(setq-default tab-width 2)
(setq-default indent-tabs-mode nil)
(setq tab-width 2)

;; (setq-default show-trailing-whitespace t)
(add-hook 'before-save-hook 'delete-trailing-whitespace)

; Get rid of the new emacs bug involving the cursor
(setq-default column-number-mode t)

;; Turn off startup message
(setq inhibit-startup-message t)

;; Goto line
(global-set-key "\M-g" 'goto-line)

;; Don't echo passwords in shell mode
(add-hook 'comint-output-filter-functions 'comint-watch-for-password-prompt)

;; Turn "yes or no" into "y or n"
(fset 'yes-or-no-p 'y-or-n-p)

;; Cycle buffer
(load "~/.emacs.d/cycle-buffer/load")

;; Long lines mode provides soft word wrapping instead of the slash c\rap
(autoload 'longlines-mode "longlines.el" "Minor mode for automatically wrapping long lines." t)

;; Use multiple terminals in one emacs session
(require 'multi-term)
(setq multi-term-program "/bin/bash")

;; Replace Meta 's' with the ability to create multiple terminals
(global-unset-key "\M-s")
(global-set-key "\M-s" 'multi-term)

;; CEDET ;;

;; (load-file "~/.emacs.d/cedet-1.0pre6/common/cedet.el")
;; (load-file "~/.emacs.d/cedet-1.1/common/cedet.el")
;; (global-ede-mode 1)                      ; Enable the Project management system
;; (semantic-load-enable-minimum-features)
;; (semantic-load-enable-code-helpers)      ; Enable prototype help and smart completion
;; (global-srecode-minor-mode 1)            ; Enable template insertion menu

;; ECB ;;

;; (add-to-list 'load-path "~/.emacs.d/ecb-2.40")
;; (require 'ecb)
;; (setq ecb-layout-name "left1")

;; windata.el
;; http://www.emacswiki.org/emacs/windata.el

(load-file "~/.emacs.d/windata.el")
(require 'windata)

;; tree-mode.el
;; http://www.emacswiki.org/emacs/download/tree-mode.el

(load-file "~/.emacs.d/tree-mode.el")
(require 'tree-mode)

;; dirtree.el
;; https://github.com/zkim/emacs-dirtree

(autoload 'dirtree "dirtree" "Add directory to tree view" t)

;;;;;;;;;;;;;;;;;;;
;;;; Languages ;;;;
;;;;;;;;;;;;;;;;;;;


;; Haskell Mode
(load "~/.emacs.d/haskell-mode-2.7.0/haskell-site-file")

;; What do these do??
(add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)
(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
(add-hook 'haskell-mode-hook 'font-lock-mode)

;; Load on .hs files
(add-to-list 'auto-mode-alist '("\\.hs$" . haskell-mode))

;; Let emacs know where ghci is
(setq haskell-program-name "/usr/bin/ghci")

;; Use hoogle inside emacs
;; NOTE (6/26/2011): I have yet to try this.. Not sure if it works
(require 'haskell-mode)
(define-key haskell-mode-map "\C-ch" 'haskell-hoogle)
;(setq haskell-hoogle-command "hoogle")

;; Ruby mode
(load "~/.emacs.d/ruby/load")

(load "~/.emacs.d/flymake")
(require 'flymake-ruby)
(add-hook 'ruby-mode-hook 'flymake-ruby-load)
(custom-set-faces
 '(flymake-errline ((((class color)) (:background "MistyRose" :foreground "black")))))


(require 'rvm)
(rvm-use-default) ;; use rvm's default ruby for the current Emacs session

(setq ruby-deep-arglist nil)
(setq ruby-deep-indent-paren nil)


;; Yaml mode
(autoload 'yaml-mode "~/.emacs.d/yaml-mode.el" nil t)
(add-to-list 'auto-mode-alist '("\\.yml$" . yaml-mode))

;; CoffeeScript Mode
(autoload 'coffee-mode "~/.emacs.d/coffee-mode.el" nil t)
(add-to-list 'auto-mode-alist '("\\.coffee$" . coffee-mode))

;; Add HTML & RHTML mode
(setq auto-mode-alist (cons '("\\.html$" . html-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.rhtml$" . html-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.erb$" . html-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.html.erb$" . html-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.iphone.erb$" . html-mode) auto-mode-alist))

;; Coffeescript mode
(add-to-list 'load-path "~/.emacs.d/coffee-mode")
(require 'coffee-mode)

;; Python mode
(load "~/.emacs.d/python/load")

;; Javascript mode
(autoload 'js2-mode "~/.emacs.d/js2-mode.elc" nil t)
(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))
(add-to-list 'auto-mode-alist '("\\.js.erb$" . js2-mode))
(setq js-indent-level 2)
(setq js2-basic-offset 2)


;; Haml mode
(autoload 'haml-mode "~/.emacs.d/haml-mode.el" nil t)
(add-to-list 'auto-mode-alist '("\\.haml$" . haml-mode))

;; Less mode
(autoload 'less-css-mode "~/.emacs.d/less-css-mode.el" nil t)
(add-to-list 'auto-mode-alist '("\\.less$" . less-css-mode))

;; Sass mode
(autoload 'sass-mode "~/.emacs.d/sass-mode.el" nil t)
(add-to-list 'auto-mode-alist '("\\.sass$" . sass-mode))

;; Scss mode
(autoload 'scss-mode "~/.emacs.d/scss-mode.el" nil t)
(add-to-list 'auto-mode-alist '("\\.scss$" . scss-mode))

;; Chuck mode
(autoload 'chuck-mode "~/.emacs.d/chuck-mode.el" nil t)
(add-to-list 'auto-mode-alist '("\\.ck$" . chuck-mode))

;; Scala mode
(let ((path "~/.emacs.d/scala-mode"))
  (setq load-path (cons path load-path))
  (load "scala-mode-auto.el"))

(defun scala-turnoff-indent-tabs-mode ()
  (setq indent-tabs-mode nil))

;; scala mode hooks
(add-hook 'scala-mode-hook 'scala-turnoff-indent-tabs-mode)

;; Php mode
(autoload 'php-mode "~/.emacs.d/php-mode.el" nil t)
(add-to-list 'auto-mode-alist '("\\.php$" . php-mode))
(add-to-list 'auto-mode-alist '("\\.inc$" . php-mode))
(add-to-list 'auto-mode-alist '("\\.module$" . php-mode))

;; Arc mode
(autoload 'arc-mode "~/.emacs.d/arc-mode.el" nil t)
(add-to-list 'auto-mode-alist '("\\.arc$" . arc-mode))

;; Nginx mode :)
(autoload 'nginx-mode "~/.emacs.d/nginx-mode.el" nil t)
(add-to-list 'auto-mode-alist '("nginx.conf$" . nginx-mode))
(add-to-list 'auto-mode-alist '("\\.nginx$" . nginx-mode))

;; CSS Mode
;; (autoload 'css-mode "~/.emacs.d/css-mode.el" nil t)
(autoload 'css-mode "~/.emacs.d/css-mode-simple.el" nil t)
(setq auto-mode-alist (cons '("\\.css\\'" . css-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.scss\\'" . css-mode) auto-mode-alist))

;; Octave Mode
(setq auto-mode-alist (cons '("\\.m\\'" . octave-mode) auto-mode-alist))

;; Octave Mode
(setq auto-mode-alist (cons '("\\.m\\'" . octave-mode) auto-mode-alist))

;; R Mode
(setq auto-mode-alist (cons '("\\.R$" . R-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.r$" . R-mode) auto-mode-alist))

(load "~/.emacs.d/ess-12.04/lisp/ess-site.el")
(setq-default inferior-S+6-program-name "Splus")
(setq-default inferior-R-program-name "R")
(ess-toggle-underscore nil)

;; Treetop mode
;; (autoload 'treetop-mode "treetop" nil t)
;; (add-to-list 'auto-mode-alist '("\\.treetop$" . treetop-mode))

;; Processing mode
(add-to-list 'load-path "~/.emacs.d/processing-mode.el")
(autoload 'processing-mode "processing-mode" "Processing mode" t)
(add-to-list 'auto-mode-alist '("\\.pde$" . processing-mode))
(setq processing-location "/home/farleyknight/src/processing-1.5/")

;; Autosave 'autosave~' and Backup '#backup#' files go into /tmp
(load "~/.emacs.d/autosave")

;; Grep commands
(autoload 'grep "igrep" "*Run `grep` PROGRAM to match REGEX in FILES..." t)
(autoload 'egrep "igrep" "*Run `egrep`..." t)
(autoload 'fgrep "igrep" "*Run `fgrep`..." t)
(autoload 'agrep "igrep" "*Run `agrep`..." t)

(autoload 'grep-find "igrep" "*Run `grep` via `find`..." t)
(autoload 'egrep-find "igrep" "*Run `egrep` via `find`..." t)
(autoload 'fgrep-find "igrep" "*Run `fgrep` via `find`..." t)
(autoload 'agrep-find "igrep" "*Run `agrep` via `find`..." t)

(defun other-window-backward ()
	"Select the previous window"
	(interactive)
	(other-window -1))


;; Split the screen to my personal taste
(defun my-split-window ()
	(interactive)
	(split-window-vertically)
	(split-window-horizontally)
	(enlarge-window 10))

(defun my-split-window-startup ()
	(my-split-window)
	(other-window 2)
	(multi-term)
	(switch-to-buffer "*terminal<1>*")
	(other-window 1))

;; (my-split-window-startup)

;; Create a hot key to switch back, if necessary
(global-unset-key "\C-a")

;; Align by regex
(global-set-key "\C-ar" 'align-regexp)

;; Start mark
(global-set-key "\C-q" 'set-mark-command)

;; For some reason, can't get regexp search to work, so mapping it to C-S-s
(global-set-key "\C-S" 'isearch-forward-regexp)

;; Disable Ctrl-Z (background this process in unix)
(global-unset-key "\C-z")

;; Copy text on insert
(global-set-key [insertchar] 'ignore)

(global-set-key [insert] 'ignore)
;; Disable the insert key..
(global-set-key [(insert)] 'ignore)

;; Create a hot key for replace-string
(global-set-key "\C-xr" 'replace-string)

;; Set C-xg to replace-regexp
(global-set-key "\C-xg" 'replace-regexp)


(custom-set-variables
 '(load-home-init-file t t))

(custom-set-faces)



;; Turn off annoying "Kill active processes?" question
(defadvice save-buffers-kill-emacs (around no-query-kill-emacs activate)
	"Prevent annoying \"Active processes exist\" query when you quit Emacs."
	(flet ((process-list ())) ad-do-it))


;; adding autotest integration
;; NOTE (6/26/2011): I don't think I use this anymore.. Might want to remove it
(require 'toggle)
(require 'autotest)

;; Indent a region
(global-set-key "\C-t" 'indent-region)

;; Rename files
(global-set-key "\C-cr" 'rename-file-and-buffer)


;; Scroll one line at a time
(setq scroll-step 1)

;; Putty
(define-key global-map [select] 'end-of-line)

;; Change C-p & C-o to cycle between windows
(global-unset-key "\C-p")
(global-set-key "\C-p"  'other-window)

(global-unset-key "\C-o")
(global-set-key "\C-o"   'other-window-backward)

(defun other-window-backward ()
  (interactive)
  (other-window -1))

(global-set-key "\M-["  'other-window)
(global-set-key "\M-]"  'other-window-backward)

;; Tail files using M-x 'tail-file'
(require 'tail)

(defun align-colons (beg end)
  (interactive "r")
  (align-regexp beg end ": \\(\\s-*\\)" 1 1 t))
